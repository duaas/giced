<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bill Claim</title>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
</head>
<body>
	<div style="margin-right:50px;margin-left:50px;font-size:15px;" >

					<table width="100%">
						<tr>
							<td>
							<div align="center">
							UNIVERSITY OF MUMBAI<br/>
							GARWARE INSTITUTE OF CAREER EDUCATION AND DEVELOPMENT<br/>
							BILL CLAIM OF VISITING FACULTY FOR THE MONTH OF<br/>
							${month} ${year}
							</div>
							</td>
						</tr>
					</table>
					<br/>
					<table  id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" >
	                    <thead>
	                    	<tr>
	                    	    <th>SR NO.</th>
	                    		<th>NAME OF THE FACULTY</th>
			                   	<th>AMOUNT</th>
			                	<th>TOTAL AMOUNT</th>
			                	<th>CHEQUE NUMBER</th>
			                	<th>SIGNATURE</th>
			                </tr>
	                   </thead>
	                   <tbody>
	                   <c:forEach var="item" items="${listReport}" varStatus="counter">
	            		<tr>
	            			<td>${counter.count}</td>
	                		<td> ${item.key}</td> 
	                		<td>
	                			<c:forEach var="valueList" items="${item.value}" >
		                    		 ${valueList}<br/>
		                		</c:forEach> 
	                		</td>
	                		<td>
	                			<c:set var="total_amount" value="0"></c:set>
	                			<c:forEach var="valueList" items="${item.value}" >
		                    		<c:set var="total_amount" value="${total_amount+valueList}"></c:set>
		                		</c:forEach> 
		                		${total_amount}
		                	</td>
		                	<td></td>
		                	<td></td>
	           			 </tr>
	        			</c:forEach>
	                   </tbody>
					</table>   

	</div>
</body>
</html>