<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Edit Faculty Assignment</title>

<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
<script>
$(document).ready(function(){
    var start_date=$('input[name="start_date"]'); //our date input has the name "date"
    var end_date=$('input[name="end_date"]'); 
    //var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
    var options={
      format: 'mm/dd/yyyy',
      //container: container,
      todayHighlight: true,
      autoclose: true,
    };
    start_date.datepicker(options);
    end_date.datepicker(options);
   
  })
     
     
function validateform(){  
	
	var assigned_theory=document.AssignmentForm.assigned_theory.value;
	var assigned_practical=document.AssignmentForm.assigned_practical.
	var start_date=document.FacultyForm.start_date.value;
	var end_date=document.FacultyForm.end_date.value;
	
	  
	if (isNaN(assigned_theory)){  
		  alert("Please enter valid Theory Hours.");  
		  return false;  
	}
	else if (isNaN(assigned_practical)){  
		  alert("Please enter valid Practical Hours.");  
		  return false;  
	}
	else if (start_date==null || start_date==""){  
		  alert("Please select Start Date.");  
		  return false;  
	}
	else if (end_date==null || end_date==""){  
		  alert("Please select End Date.");  
		  return false;  
	}else if(end_date<start_date){
		alert("Please select valid Dates");  
		  return false; 
	}
	$('#hdnPending').val(assigned_duration);
  
}   

    </script>
</head>
<body>

<c:url var="post_url"  value="/assignments/edit" />
<form:form action="${post_url}" modelAttribute="assignment" method="post" 
class="form-horizontal" name="AssignmentForm" onsubmit="return validateform()">
	    <div class="row" >
	    		<div class="col-md-8 col-md-offset-1">
	 <fieldset>
      	<legend>Course-Faculty Assignment</legend>
      	
          <div class="form-group">
            <div class="col-sm-4">
              <form:label path="assignment_id">
			  	Assignment Id
			  </form:label><br/>
			  <form:hidden path="assignment_id" name="assignment_id" />
		        <c:set var="sub_test" value="${fn:replace(assignment_id,'_','/')}"></c:set>
				<c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
				${sub}
			  
            </div>
           </div>
           
           <form:hidden path="course_id"/>
           <form:hidden path="semister"/>
           <form:hidden path="subject_id"/>
           <form:hidden path="faculty_id"/>
           
          <div class="form-group">
           	   <div class="col-sm-4">
	              <form:label path="assigned_theory">
					 Theory Hours<span class="required">*</span>
				  </form:label>
				 <form:input path="assigned_theory" name="assigned_theory" class="form-control required"/>
	            </div> 
	           <div class="col-sm-4">
	              <form:label path="assigned_practical">
					 Practical Hours<span class="required">*</span>
				  </form:label>
				 <form:input path="assigned_practical" name="assigned_practical" class="form-control required"/>
	            </div> 
          </div>
          <div class="form-group">
          	 <div class="col-sm-5">
              <form:label path="start_date">
				 Start Date<span class="required">*</span>
			  </form:label>
			  <form:input path="start_date" name="start_date" class="form-control" id="start_date" value="${start_date}" type="text"/>
            </div> 
          	<div class="col-sm-5">
             <form:label path="end_date">
				 End Date<span class="required">*</span>
			  </form:label>
			  <form:input path="end_date" name="end_date" class="form-control" id="end_date" value="${end_date}" type="text"/>
            </div> 
          </div>
          
          <div class="form-group">
	           <div class="col-sm-4">
	              <form:label path="no_of_visits">
					 No of Visits<span class="required">*</span>
				  </form:label>
				 <form:input path="no_of_visits" name="no_of_visits" class="form-control required"/>
	            </div> 
          </div>
         
          <legend></legend>
		<c:if test="${not empty errorMsg}">
    		<div class="alert alert-danger">
    							<c:out value="${errorMsg}"/>
    		</div>
    	</c:if>
          
          <!-- Command -->
          <div class="form-group">
            <div class="col-sm-5 col-sm-offset-1">
              <div class="pull-left">
                <button type="submit" class="btn btn-primary">SAVE</button>
                <a class="btn btn-default" href="<c:url value="/assignments"/>">CANCEL</a>
              </div>
            </div>
          </div>
          
      </fieldset>
				</div>
			</div>
</form:form>
</body>
</html>
