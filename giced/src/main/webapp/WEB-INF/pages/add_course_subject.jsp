<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add New Course</title>
<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
<script>
function validateform(){  
	
	var sem=document.CourseSubjectForm.sem.value;  
	var subject1=document.CourseSubjectForm.subject1.value;  
	var subject2=document.CourseSubjectForm.subject2.value;
	var subject3=document.CourseSubjectForm.subject3.value;
	var subject4=document.CourseSubjectForm.subject4.value;
	var subject5=document.CourseSubjectForm.subject5.value;
	var subject6=document.CourseSubjectForm.subject6.value;
	var subject7=document.CourseSubjectForm.subject7.value;
	var subject8=document.CourseSubjectForm.subject8.value;
	
	if(subject1==null) {}
		alert("Please select Subjects for Semester 1.");  
		return false;
	}
	if(subject2==null) {}
		alert("Please select Subjects for Semester 2.");  
		return false;
	}
	if(sem > 2){
		if(subject3==null) {}
			alert("Please select Subjects for Semester 3.");  
			return false;
		}
		if(subject4==null) {}
			alert("Please select Subjects for Semester 4.");  
			return false;
		}
		if(sem > 4){
			if(subject5==null) {}
				alert("Please select Subjects for Semester 5.");  
				return false;
			}
			if(subject6==null) {}
				alert("Please select Subjects for Semester 6.");  
				return false;
			}
			if(sem > 6){
				if(subject7==null) {}
					alert("Please select Subjects for Semester 7.");  
					return false;
				}
				if(subject8==null) {}
					alert("Please select Subjects for Semester 8.");  
					return false;
				}				
			}
		}
	}
	  

}
</script>
</head>
<body>

<c:url var="post_url"  value="/courses/addCourseSubject" />
<form action="${post_url}"  method="post" class="form-horizontal" name="CourseSubjectForm" onsubmit="return validateform()"   >
			<div class="row" >
	    		<div class="col-md-8 col-md-offset-1">
	      			<fieldset>
	      				<legend>Add Subjects</legend>
				      	<div class="form-group">
				            <div class="col-sm-4">
				            <input type="hidden" name="course_id" value="${course_id}"/>
				            	<spring:message text="Course"/><br/>
				                <input value="${course_name}"  class="form-control" readonly="readonly"/>
				            </div>
				            <div class="col-sm-4">
				            	<spring:message text="No. of Semesters"/><br/>
				                <input value="${sem}" name="sem" class="form-control" readonly="readonly"/>
				            </div>
			          	</div>
			          				          	
			          	
				          	<div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Semester 1"/><br/>
					          		<select multiple="multiple"  name="subject1" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4">
					            	<spring:message text="Semester 2"/><br/>
					          		<select multiple="multiple"  name="subject2" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
							</div>
			          	
			          	<c:if test="${(sem==4)||(sem==6)||(sem==8)}">
			          	    <div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Semester 3"/><br/>
					          		<select multiple="multiple"  name="subject3" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4">
					            	<spring:message text="Semester 4"/><br/>
					          		<select multiple="multiple"  name="subject4" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
							</div>
						</c:if>
			          	
			          	<c:if test="${(sem==6)||(sem==8)}">
				          	<div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Semester 5"/><br/>
					          		<select multiple="multiple"  name="subject5" class="form-control" >
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
								 <div class="col-sm-4">
					            	<spring:message text="Semester 6"/><br/>
					          		<select multiple="multiple"  name="subject6" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
									   	</c:forEach>
									</select>
								</div>
							</div>
			          	</c:if>
			          
			          	<c:if test="${sem==8}">
				          	<div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Semester 7"/><br/>
					          		<select multiple="multiple"  name="subject7" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
								<div class="col-sm-4">
					            	<spring:message text="Semester 8"/><br/>
					          		<select multiple="multiple"  name="subject8" class="form-control">
										<c:forEach var="list" items="${listSubject}">
										       <option value="${list.subject_id}">${list.subject_name}</option> 
										</c:forEach>
									</select>
								</div>
							</div>
			          	</c:if>
			          	
				      	<div class="form-group">
				            <div class="col-sm-5 col-sm-offset-1">
				              <div class="pull-left">
				                <button type="submit" class="btn btn-primary">SAVE</button>
				                <a class="btn btn-default" href="<c:url value="/courses"/>">CANCEL</a>
				              </div>
				            </div>
			           </div>
	      			</fieldset>
				</div>
			</div>
			 <input type="hidden"  name="${_csrf.parameterName}"   value="${_csrf.token}"/>
</form>
</body>
</html>