<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Show Assignment Details</title>

<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
</head>
<body>


<form:form class="form-horizontal" >
  <div class="row" >
    <div class="col-md-8 col-md-offset-1">
      <fieldset>
	      				<legend>Assignment Details</legend>
	      				<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Assignment Id: "/>
				            	<c:set var="sub_test" value="${fn:replace(assign.assignment_id,'_','/')}"></c:set>
					            <c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
					            <spring:message text="${sub}"/>
				            </div>
			          	</div>
				      	<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Course: "/>
				              	<spring:message text="${course}"/>
				            </div>
			          	</div>
			          	<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Semister: "/>
				              	<spring:message text="${assign.semister}"/>
				            </div>
			          	</div>
			          	<div class="form-group">
			          	<div class="col-sm-4">
				            	<spring:message text="Subject Code: "/>
				              	<spring:message text="${subject_code}"/>
				            </div>
				            <div class="col-sm-4">
				            	<spring:message text="Subject: "/>
				              	<spring:message text="${subject}"/>
				            </div>
			          	</div>
			          	<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Faculty: "/>
				              	<spring:message text="${faculty}"/>
				            </div>
			          	</div>
			          	
			          		<div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Assigned Theory Hours: "/><br/>
					          		<spring:message text="${assign.assigned_theory}"/>
								</div>
								<div class="col-sm-4">
					            	<spring:message text="Pending Theory Hours: "/><br/>
					          		<spring:message text="${assign.pending_theory}"/>
								</div>
							</div>
			          	
			          	<div class="form-group">
					            <div class="col-sm-4">
					            	<spring:message text="Assigned Practical Hours: "/><br/>
					          		<spring:message text="${assign.assigned_practical}"/>
								</div>
								<div class="col-sm-4">
					            	<spring:message text="Pending Practical Hours: "/><br/>
					          		<spring:message text="${assign.pending_practical}"/>
								</div>
							</div>
			          	
			          	   <div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Start Date: "/>
				            	<fmt:formatDate value="${assign.start_date}" pattern="dd-MM-yyyy" var="start_date" />
			         			<spring:message text="${start_date}"/>
				            </div>
				            <div class="col-sm-4">
				            	<spring:message text="End Date: "/>
				            	<fmt:formatDate value="${assign.end_date}" pattern="dd-MM-yyyy" var="end_date" />
			         			<spring:message text="${end_date}"/>
				            </div>
			          	</div>
			          	
			          	<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="No of Visits: "/>
				              	<spring:message text="${assign.no_of_visits}"/>
				            </div>
			          	</div>
			          	
			          	<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Created By: "/>
				              	<spring:message text="${assign.created_by}"/>
				            </div>
				            <div class="col-sm-4">
				            	<spring:message text="Created Date: "/>
				            	<spring:message text="${assign.created_date}"/>
				            </div>
			          	</div>
					
					<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Status: "/>
				              	<spring:message text="${assign.approval_status}"/>
				            </div>
			          	</div>
			
						<div class="form-group">
				            <div class="col-sm-4">
				            	<spring:message text="Approved/Rejected By: "/>
				              	<spring:message text="${assign.approved_by}"/>
				            </div>
				            <div class="col-sm-4">
				            	<spring:message text="Approved/Rejected Date: "/>
				            	<spring:message text="${assign.approved_date}"/>
				            </div>
			          	</div>
				      	<div class="form-group">
            <div class="col-sm-5 col-sm-offset-1">
              <div class="pull-left">
                <a class="btn btn-default" href="<c:url value="/assignments"/>">CANCEL</a>
              </div>
            </div>
          </div>
	      			</fieldset>
    </div><!-- /.col-lg-12 -->
</div><!-- /.row -->
	
</form:form>
</body>
</html>