package com.giced.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "assignment")
public class Assignment {
	
    @Id
    @Column(name = "assignment_id")
    private String assignment_id;
    
    @Column(name = "course_id")
    private String course_id;
    
    @Column(name = "semister")
    private int semister;
    
    @Column(name = "subject_id")
    private String subject_id;
    
    @Column(name = "faculty_id")
    private String faculty_id;
    
    @Column(name = "assigned_theory")
    private String assigned_theory;
    
    @Column(name = "assigned_practical")
    private String assigned_practical;
    
    @Column(name = "pending_theory")
    private String pending_theory;
    
    @Column(name = "pending_practical")
    private String pending_practical;
    
    @Column(name = "start_date")
    private Date start_date;
    
    @Column(name = "end_date")
    private Date end_date;
    
    @Column(name = "no_of_visits")
    private int no_of_visits;
    
    @Column(name = "created_date")
    private String created_date;
    
	@Column(name = "created_by")
    private String created_by;
	
	@Column(name = "approved_date")
    private String approved_date;
	
	@Column(name = "approved_by")
    private String approved_by;
	
	@Column(name = "approval_status")
    private String approval_status;
	

	public String getCreated_date() {
		return created_date;
	}

	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}

	public String getCreated_by() {
		return created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public String getApproved_date() {
		return approved_date;
	}

	public void setApproved_date(String approved_date) {
		this.approved_date = approved_date;
	}

	public String getApproved_by() {
		return approved_by;
	}

	public void setApproved_by(String approved_by) {
		this.approved_by = approved_by;
	}

	public String getApproval_status() {
		return approval_status;
	}

	public void setApproval_status(String approval_status) {
		this.approval_status = approval_status;
	}

	public int getSemister() {
		return semister;
	}

	public void setSemister(int semister) {
		this.semister = semister;
	}



	public int getNo_of_visits() {
		return no_of_visits;
	}

	public void setNo_of_visits(int no_of_visits) {
		this.no_of_visits = no_of_visits;
	}

	public String getAssignment_id() {
		return assignment_id;
	}

	public void setAssignment_id(String assignment_id) {
		this.assignment_id = assignment_id;
	}

	public String getCourse_id() {
		return course_id;
	}

	public void setCourse_id(String course_id) {
		this.course_id = course_id;
	}

	public String getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(String subject_id) {
		this.subject_id = subject_id;
	}

	public String getFaculty_id() {
		return faculty_id;
	}

	public void setFaculty_id(String faculty_id) {
		this.faculty_id = faculty_id;
	}


	public String getAssigned_theory() {
		return assigned_theory;
	}

	public void setAssigned_theory(String assigned_theory) {
		this.assigned_theory = assigned_theory;
	}

	public String getAssigned_practical() {
		return assigned_practical;
	}

	public void setAssigned_practical(String assigned_practical) {
		this.assigned_practical = assigned_practical;
	}

	public String getPending_theory() {
		return pending_theory;
	}

	public void setPending_theory(String pending_theory) {
		this.pending_theory = pending_theory;
	}

	public String getPending_practical() {
		return pending_practical;
	}

	public void setPending_practical(String pending_practical) {
		this.pending_practical = pending_practical;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
    
    
}
