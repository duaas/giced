package com.giced.model;


public class AttendanceMap {
	
	private String assignment_id;
	
	private String date;
	
	private String assigned_theory;
    
    private String assigned_practical;

	public String getAssignment_id() {
		return assignment_id;
	}

	public void setAssignment_id(String assignment_id) {
		this.assignment_id = assignment_id;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAssigned_theory() {
		return assigned_theory;
	}

	public void setAssigned_theory(String assigned_theory) {
		this.assigned_theory = assigned_theory;
	}

	public String getAssigned_practical() {
		return assigned_practical;
	}

	public void setAssigned_practical(String assigned_practical) {
		this.assigned_practical = assigned_practical;
	}
    
   
}
