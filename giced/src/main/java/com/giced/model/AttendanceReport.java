package com.giced.model;


public class AttendanceReport {
	

	//private long id;

	private String assignment_id;
	
	private String course;
	
	private String subject;
	
	private String faculty;
	
	private int sem;
	
	private String sub_code;
	
	public String getSub_code() {
		return sub_code;
	}

	public void setSub_code(String sub_code) {
		this.sub_code = sub_code;
	}

	private String date;
	
	private String approval_date;
	
	public String getApproval_date() {
		return approval_date;
	}

	public void setApproval_date(String approval_date) {
		this.approval_date = approval_date;
	}

	private String assigned_theory;
    
    private String assigned_practical;
    
    private String pending_theory;
    
    private String pending_practical;
    
    private String prev_bal_theory;
    
    private String prev_bal_practical;
    
    private String completed_theory;
    
    private String completed_practical;
    
    private double total_theory;
    
    private double total_prac;
    
    public double getTotal_theory() {
		return total_theory;
	}

	public void setTotal_theory(double total_theory) {
		this.total_theory = total_theory;
	}

	public double getTotal_prac() {
		return total_prac;
	}

	public void setTotal_prac(double total_prac) {
		this.total_prac = total_prac;
	}

	private int month;
    
    private int year;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getAssignment_id() {
		return assignment_id;
	}

	public void setAssignment_id(String assignment_id) {
		this.assignment_id = assignment_id;
	}

	public String getCourse() {
		return course;
	}

	public void setCourse(String course) {
		this.course = course;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public int getSem() {
		return sem;
	}

	public void setSem(int sem) {
		this.sem = sem;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getAssigned_theory() {
		return assigned_theory;
	}

	public void setAssigned_theory(String assigned_theory) {
		this.assigned_theory = assigned_theory;
	}

	public String getAssigned_practical() {
		return assigned_practical;
	}

	public void setAssigned_practical(String assigned_practical) {
		this.assigned_practical = assigned_practical;
	}

	public String getPending_theory() {
		return pending_theory;
	}

	public void setPending_theory(String pending_theory) {
		this.pending_theory = pending_theory;
	}

	public String getPending_practical() {
		return pending_practical;
	}

	public void setPending_practical(String pending_practical) {
		this.pending_practical = pending_practical;
	}

	public String getPrev_bal_theory() {
		return prev_bal_theory;
	}

	public void setPrev_bal_theory(String prev_bal_theory) {
		this.prev_bal_theory = prev_bal_theory;
	}

	public String getPrev_bal_practical() {
		return prev_bal_practical;
	}

	public void setPrev_bal_practical(String prev_bal_practical) {
		this.prev_bal_practical = prev_bal_practical;
	}

	public String getCompleted_theory() {
		return completed_theory;
	}

	public void setCompleted_theory(String completed_theory) {
		this.completed_theory = completed_theory;
	}

	public String getCompleted_practical() {
		return completed_practical;
	}

	public void setCompleted_practical(String completed_practical) {
		this.completed_practical = completed_practical;
	}

    
}
