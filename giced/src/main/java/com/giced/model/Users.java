package com.giced.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {
	@Id
    @Column(name = "user_name")
    private String user_name;
    
    @Column(name = "user_password")
    private String user_password;
    
    @Column(name = "role_id")
    private String role_id;
    
    @Column(name = "user_details")
    private String user_details;
    
    @Column(name = "user_email")
    private String user_email;
    
    public String getUser_details() {
		return user_details;
	}

	public void setUser_details(String user_details) {
		this.user_details = user_details;
	}

	public String getUser_email() {
		return user_email;
	}

	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getUser_password() {
		return user_password;
	}

	public void setUser_password(String user_password) {
		this.user_password = user_password;
	}

	public String getRole_id() {
		return role_id;
	}

	public void setRole_id(String role_id) {
		this.role_id = role_id;
	}

	
}
