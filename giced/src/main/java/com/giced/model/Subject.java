package com.giced.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subject")
public class Subject {
	@Id
    @Column(name = "subject_id")
    private String subject_id;

    @Column(name = "subject_name")
    private String subject_name;
    
    @Column(name = "theory_hours")
    private int theory_hours;
    
    @Column(name = "practical_hours")
    private int practical_hours;

	public String getSubject_id() {
		return subject_id;
	}

	public void setSubject_id(String subject_id) {
		this.subject_id = subject_id;
	}

	public String getSubject_name() {
		return subject_name;
	}

	public void setSubject_name(String subject_name) {
		this.subject_name = subject_name;
	}

	public int getTheory_hours() {
		return theory_hours;
	}

	public void setTheory_hours(int theory_hours) {
		this.theory_hours = theory_hours;
	}

	public int getPractical_hours() {
		return practical_hours;
	}

	public void setPractical_hours(int practical_hours) {
		this.practical_hours = practical_hours;
	}



	
	
}
