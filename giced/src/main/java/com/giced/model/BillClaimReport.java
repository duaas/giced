package com.giced.model;


public class BillClaimReport {
	

	
	private String assignment_id;
	
	private String faculty;
	
	private double amount;
    
    /*private double total_amount;*/
    
    private int month;
    
    private int year;

	/*public BillClaimReport(String faculty, double amount) {
		this.faculty=faculty;
		this.amount=amount;
	}
*/
	public String getAssignment_id() {
		return assignment_id;
	}

	public void setAssignment_id(String assignment_id) {
		this.assignment_id = assignment_id;
	}

	public String getFaculty() {
		return faculty;
	}

	public void setFaculty(String faculty) {
		this.faculty = faculty;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
    
	
	
}
