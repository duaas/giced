package com.giced.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.giced.model.Course_Subject;



@Repository
public class CourseSubjectDaoImpl implements CourseSubjectDao {

	private SessionFactory sessionFactory;
   
    @Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void addCourseSubject(Course_Subject crs_sub) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(crs_sub);
	}

	@Override
	public void updateCourseSubject(Course_Subject crs_sub) {
		Session session = sessionFactory.getCurrentSession();
		session.update(crs_sub);
	}

	@Override
	public void removeCourseSubject(int id) {
		Session session = sessionFactory.getCurrentSession();
		Course_Subject crs_sub = (Course_Subject) session.get(Course_Subject.class, id);
        if (crs_sub != null)
            session.delete(crs_sub);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Course_Subject> getCourseSubject(String course_id, int sem) {
		Session session = sessionFactory.getCurrentSession();
		Query query=null;
		if(sem==0) {
			query = session.createQuery("FROM Course_Subject WHERE course_id = :course_id");
			query.setParameter("course_id", course_id);
		}
		else {
			query = session.createQuery("FROM Course_Subject WHERE course_id = :course_id AND semister = :sem");
	        query.setParameter("course_id", course_id);
	        query.setParameter("sem", sem);
		}
        List<Course_Subject> crs_sub = query.list();
        return crs_sub;
	}

	@Override
	@SuppressWarnings("unchecked")
	public String getSubjectCode(String course_id, int sem, String subject_id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Course_Subject WHERE course_id = :course_id AND semister = :sem AND subject_id = :subject_id");
		query.setParameter("course_id", course_id);
		query.setParameter("sem", sem);
		query.setParameter("subject_id", subject_id);
		List<Course_Subject> listCrsSub = query.list();
		String subject_code=null;
        for(Course_Subject crs:listCrsSub ) {
        	subject_code=crs.getSubject_code();
        }
        return subject_code;
	}

	@Override
	public void deleteCourseSubject(String course_id) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Course_Subject WHERE course_id = :course_id");
		query.setParameter("course_id", course_id);
		query.executeUpdate();	
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boolean existsCourseSubject(String course_id) {
		Session session = sessionFactory.getCurrentSession();
		Query query= session.createQuery("FROM Course_Subject WHERE course_id = :course_id");
	        query.setParameter("course_id", course_id);
        List<Course_Subject> crs_sub = query.list();
        return crs_sub.isEmpty();
	}

    
}
