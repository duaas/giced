package com.giced.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.giced.model.Payment;



@Repository
public class PaymentDaoImpl implements PaymentDao {

	private SessionFactory sessionFactory;
      
    @Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

	@Override
	public void addPayment(Payment payment) {
		Session session = sessionFactory.getCurrentSession();
		session.persist(payment);
	}

	@Override
	public void updatePayment(Payment payment) {
		Session session = sessionFactory.getCurrentSession();
        session.update(payment);
	}

	@Override
	public void removePayment(int id) {
		Session session = sessionFactory.getCurrentSession();
		Payment payment = (Payment) session.get(Payment.class, id);
        if (payment != null)
            session.delete(payment);
	}

	@Override
	public Payment getPayment(int id) {
		Session session = sessionFactory.getCurrentSession();
		Payment payment = (Payment) session.get(Payment.class, id);
        return payment;
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Payment> getAllPayments() {
		Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("FROM Payment");
        List<Payment> listPayment = query.list();
        return listPayment;
	}

	@SuppressWarnings("unchecked")
	@Override
	public int getPaymentAmount(String payment_type) {
		Session session = sessionFactory.getCurrentSession();
		Query query = session.createQuery("FROM Payment WHERE payment_type = :payment_type");
		query.setParameter("payment_type", payment_type);
		List<Payment> listPay = query.list();
		int amount=0;
        for(Payment pay:listPay ) {
        	amount=pay.getPayment_amount();
        }
        return amount;
	}

	


}
