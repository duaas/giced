package com.giced.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.giced.model.Assignment;
import com.giced.model.Attendance;
import com.giced.model.Faculty;
import com.giced.model.Subject;
import com.giced.service.AssignmentService;
import com.giced.service.AttendanceService;
import com.giced.service.CourseService;
import com.giced.service.FacultyService;
import com.giced.service.PaymentService;
import com.giced.service.SubjectService;

@Controller
public class AttendanceController {
	 
	private AssignmentService assignmentService;
	private AttendanceService attendanceService;
	private CourseService courseService;
	private SubjectService subjectService;
	private FacultyService facultyService;
	private PaymentService paymentService;
	
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@Autowired
	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}

	@Autowired
	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	@Autowired
	public void setCourseService(CourseService courseService) {
		this.courseService = courseService;
	}

	@Autowired
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Autowired
	public void setFacultyService(FacultyService facultyService) {
		this.facultyService = facultyService;
	}

	//Show Attendance List
	@RequestMapping(value = "attendances", method = RequestMethod.GET)
	public String listAttendance( Model model) {
	        model.addAttribute("listAttendance", attendanceService.getAllAttendance());
	        model.addAttribute("listAssignment", assignmentService.getAllAssignments());
	        model.addAttribute("listCourse", courseService.getAllCourses());
	        model.addAttribute("listSubject", subjectService.getAllSubjects());
	        model.addAttribute("listFaculty", facultyService.getAllFaculties());
	        AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
	        model.addAttribute("user_role", auth.getAuthorities());
	        
	        return "list_attendance";
	 }
	
	//Show Add Attendance Form
	@RequestMapping(value = "add_attendance", method = RequestMethod.GET)
	public String addAttendanceForm(Model model) {
		model.addAttribute("assignment", new Assignment());
		model.addAttribute("attendance", new Attendance());
		model.addAttribute("listCourse", courseService.getAllCourses());
		return "add_attendance";
	}
		
	
	@RequestMapping(value="getSubject1.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody  List<Subject> getSubject(@RequestParam(value = "course_id", required = true) String course_id,
			ModelMap modelMap ){
		List<Assignment> listAssign=assignmentService.getAssignmentforData(course_id, "NONE", "NONE");
		List<Subject> listSubject=new ArrayList<Subject>();
		for(Assignment a:listAssign) {
			String s=a.getSubject_id();
			listSubject.add(subjectService.getSubject(s));
		}
		return listSubject;
	}
	

	@RequestMapping(value="getFaculty.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody  List<Faculty> getFaculty(@RequestParam(value = "course_id", required = true) String course_id,
			@RequestParam(value = "subject_id", required = true) String subject_id,
			ModelMap modelMap ){
		List<Assignment> listAssign=assignmentService.getAssignmentforData(course_id, subject_id, "NONE");
		List<Faculty> listFaculty=new ArrayList<Faculty>();
		for(Assignment a:listAssign) {
			String f=a.getFaculty_id();
			listFaculty.add(facultyService.getFaculty(f));
		}
		return listFaculty;
	}
	

	@RequestMapping(value="getAssignment.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody  List<Assignment> getAssignment(@RequestParam(value = "course_id", required = true) String course_id,
			@RequestParam(value = "subject_id", required = true) String subject_id,
			@RequestParam(value = "faculty_id", required = true) String faculty_id,
			ModelMap modelMap ){
		List<Assignment> listAssign=assignmentService.getAssignmentforData(course_id, subject_id, faculty_id);
		return listAssign;
	}
	
	//Add Assignment
	@RequestMapping(value = "attendances/add", method = RequestMethod.POST)
    public String addAttendance(@ModelAttribute("attendance") Attendance attendance, Model model) {
		DateFormat format;
		Date d1= null;
		Date d2=null;
		try {
			format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
				        
				d1 = format.parse(attendance.getIn_time());
				d2 =  format.parse(attendance.getOut_time());
			
	        long diff = d2.getTime() - d1.getTime();
	        long diffMinutes = diff / (60 * 1000) % 60 ;
	        long diffHours = diff / (60 * 60 * 1000) % 24;
	        if (diffMinutes==30) diffMinutes=50;
	        attendance.setCompleted_hours(diffHours+"."+diffMinutes);
	        double diff_completed=Double.parseDouble(attendance.getCompleted_hours());
	        double amount=0;
	        if(attendance.getAssignment_type().equals("Theory")) {
	        	amount=(double)paymentService.getPaymentAmount("Theory")*diff_completed;		
        	}
        	else if(attendance.getAssignment_type().equals("Practical")){
        		amount=(double)paymentService.getPaymentAmount("Practical")*diff_completed;
        	}
	        /*Assignment assign=assignmentService.getAssignment(attendance.getAssignment_id());
	        if(attendance.getAssignment_type().equals("Theory")) attendance.setBalance_hours(assign.getPending_theory());
	        else attendance.setBalance_hours(assign.getPending_practical());*/
	        attendance.setBalance_hours("-");
	        attendance.setAmount(amount);
	        attendanceService.addAttendance(attendance);
	        
	        setPendingDuration(attendance.getAssignment_id());
	        
	        Assignment assign=assignmentService.getAssignment(attendance.getAssignment_id());
	        if(attendance.getAssignment_type().equals("Theory")) attendance.setBalance_hours(String.valueOf(Double.parseDouble(assign.getPending_theory())+diff_completed));
	        else if(attendance.getAssignment_type().equals("Practical")) attendance.setBalance_hours(String.valueOf(Double.parseDouble(assign.getPending_practical())+diff_completed));
	        attendanceService.updateAttendance(attendance);
	           
		}
        catch(Exception ex) {
        	model.addAttribute("errorMsg", ex.getMessage());
        	model.addAttribute("listCourse", courseService.getAllCourses());
			return "add_attendance";
        }
		return "redirect:/attendances";
		
    }
		
	private void setPendingDuration(String assignment_id) {
		Assignment assignment=assignmentService.getAssignment(assignment_id);
        List<Attendance> listAt=attendanceService.getAttendanceforAssignment(assignment_id);
        
        double diff_completed_theory = 0;
        double diff_completed_practical=0;
        for(Attendance at:listAt) {
        	if(at.getAssignment_type().equals("Theory") ){
        		double l_completed= Double.parseDouble(at.getCompleted_hours());
        		diff_completed_theory=diff_completed_theory+l_completed;
        		//at.setBalance_hours(assignment.getPending_theory()+diff_completed_theory);
        	}
        	else if(at.getAssignment_type().equals("Practical")){
        		double l_completed= Double.parseDouble(at.getCompleted_hours());
        		diff_completed_practical=diff_completed_practical+l_completed;
        		//at.setBalance_hours(assignment.getPending_practical()+diff_completed_practical);
        	}
        	//attendanceService.updateAttendance(at);
        }
        if(diff_completed_theory>0) {
        	double l_assigned=Double.parseDouble(assignment.getAssigned_theory());
            double l_pending=l_assigned - diff_completed_theory;
            assignment.setPending_theory(String.valueOf(l_pending));
        }
        if(diff_completed_practical>0) {
        	double l_assigned=Double.parseDouble(assignment.getAssigned_practical());
            double l_pending=l_assigned - diff_completed_practical;
            assignment.setPending_practical(String.valueOf(l_pending));
        }
        
        assignmentService.updateAssignment(assignment);
	}
	
	//Show Edit Attendance Form
		@RequestMapping("editAttendance/{id}")
	    public String editAttendanceForm(@PathVariable("id") long id, Model model) {
			Attendance attendance=attendanceService.getAttendance(id);
			model.addAttribute("attendance", attendance);
			Date date=attendance.getDate();
			SimpleDateFormat simpleDate = new SimpleDateFormat("MM/dd/yyyy");
			String formattedDate = simpleDate.format(date);
			model.addAttribute("date",formattedDate);
			
		    return "edit_attendance";
	    }
		
		
		//Edit Assignment Details
		@RequestMapping(value = "attendances/edit", method = RequestMethod.POST)
	    public String editAttendance(@ModelAttribute("attendance") Attendance attendance,Model model) {
			try {
				DateFormat format = new SimpleDateFormat("HH:mm", Locale.ENGLISH);
	            Date d1 = null;
	            Date d2 = null;
				d1 = format.parse(attendance.getIn_time());
				d2 = format.parse(attendance.getOut_time());
				
				
				long diff = d2.getTime() - d1.getTime();
	            long diffMinutes = diff / (60 * 1000) % 60 ;
	            long diffHours = diff / (60 * 60 * 1000) % 24;
	            if (diffMinutes==30) diffMinutes=50;
	            attendance.setCompleted_hours(diffHours+"."+diffMinutes);
	            double diff_completed=Double.parseDouble(attendance.getCompleted_hours());
		        double amount=0;
		        Assignment assign=assignmentService.getAssignment(attendance.getAssignment_id());
		        if(attendance.getAssignment_type().equals("Theory")) {
		        	amount=(double)paymentService.getPaymentAmount("Theory")*diff_completed;
		        	//attendance.setBalance_hours(assign.getPending_theory());
	        	}
	        	else if(attendance.getAssignment_type().equals("Practical")) {
	        		amount=(double)paymentService.getPaymentAmount("Practical")*diff_completed;
	        		//attendance.setBalance_hours(assign.getPending_practical());
	        	}
		        //attendance.setBalance_hours("-");
		        attendance.setAmount(amount);
		        
	            attendanceService.updateAttendance(attendance);
	            setPendingDuration(attendance.getAssignment_id());
	            if(attendance.getAssignment_type().equals("Theory")) attendance.setBalance_hours(String.valueOf(Double.parseDouble(assign.getPending_theory())+diff_completed));
		        else if(attendance.getAssignment_type().equals("Practical")) attendance.setBalance_hours(String.valueOf(Double.parseDouble(assign.getPending_practical())+diff_completed));
		        
		        attendanceService.updateAttendance(attendance);
	            
	        }
			catch(Exception ex) {
	        	model.addAttribute("errorMsg", ex.getMessage());
	        	return "redirect:/editAttendance/{"+attendance.getId()+"}";
	        }
	        return "redirect:/attendances";
	    }
		
		//Delete Assignment
		@RequestMapping("removeAttendance/{id}")
	    public String removeAttendance(@PathVariable("id") long id) {
			Attendance attendance=attendanceService.getAttendance(id);
			String assignment_id=attendance.getAssignment_id();
			attendanceService.removeAttendance(id);
			setPendingDuration(assignment_id);
	        return "redirect:/attendances";
	    }
		
		
}
