package com.giced.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.giced.model.AssignedSubject;
import com.giced.model.Assignment;
import com.giced.model.Course;
import com.giced.model.Course_Subject;
import com.giced.model.Faculty;
import com.giced.model.Subject;
import com.giced.service.AssignmentService;
import com.giced.service.CourseService;
import com.giced.service.CourseSubjectService;
import com.giced.service.FacultyService;
import com.giced.service.PaymentService;
import com.giced.service.SubjectService;

@Controller
public class AssignmentController {
	 
	private AssignmentService assignmentService;
	private CourseService courseService;
	private SubjectService subjectService;
	private FacultyService facultyService;
	private CourseSubjectService crs_subService;
	private PaymentService paymentService;

	
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@Autowired	
	public void setCrs_subService(CourseSubjectService crs_subService) {
		this.crs_subService = crs_subService;
	}

	@Autowired
	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	@Autowired
	public void setCourseService(CourseService courseService) {
		this.courseService = courseService;
	}

	@Autowired
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Autowired
	public void setFacultyService(FacultyService facultyService) {
		this.facultyService = facultyService;
	}

	//Show Assignment List
	@RequestMapping(value = "assignments", method = RequestMethod.GET)
	public String listAssignments(Model model) {
	        model.addAttribute("listApprovedAssignment",  assignmentService.getAssignmentforStatus("APPROVED"));
	        model.addAttribute("listNewAssignment", assignmentService.getAssignmentforStatus("NEW"));
	        List<Assignment> listAssignment=new ArrayList<Assignment>();
	        List<Assignment> listNew=assignmentService.getAssignmentforStatus("NEW");
	        List<Assignment> listRejected=assignmentService.getAssignmentforStatus("REJECTED");
	        for(Assignment a:listNew) {
	        	listAssignment.add(a);
	        }
	        for(Assignment a:listRejected) {
	        	listAssignment.add(a);
	        }
	        model.addAttribute("listAssignment", listAssignment);
	        model.addAttribute("listCourse", courseService.getAllCourses());
	        model.addAttribute("listFaculty", facultyService.getAllFaculties());
	        model.addAttribute("listSubject", subjectService.getAllSubjects());
	        AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
	        model.addAttribute("user_role", auth.getAuthorities());
	        return "list_assignment";
	 }
	
		//Show Add Assignment Form
		@RequestMapping(value = "add_assignment", method = RequestMethod.GET)
		public String addAssignmentForm(Model model) {
			model.addAttribute("assignment", new Assignment());
			model.addAttribute("listCourse", courseService.getAllCourses());
			model.addAttribute("listFaculty", facultyService.getAllFaculties());
			
			return "add_assignment";
		}
		
		
		
	
	@RequestMapping(value="getSubject.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody  List<AssignedSubject> getSubject(@RequestParam(value = "course_id", required = true) String course_id,
			@RequestParam(value = "semister", required = true) int sem,
			ModelMap modelMap ){
		List<Course_Subject> listCrsSub=crs_subService.getCourseSubject(course_id, sem);
		
		List<AssignedSubject> listSubject=new ArrayList<AssignedSubject>();
		for(Course_Subject cs:listCrsSub ) {
			String s=cs.getSubject_id();
			Subject sub=subjectService.getSubject(s);
			AssignedSubject ass_sub=new AssignedSubject();
			ass_sub.setSubject_code(cs.getSubject_code());
			ass_sub.setSubject_id(s);
			ass_sub.setSubject_name(sub.getSubject_name());
			listSubject.add(ass_sub);
		}
	    return listSubject;
	}
	
	@RequestMapping(value="getSemister.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody  int getSemister(@RequestParam(value = "course_id", required = true) String course_id,
			ModelMap modelMap ){
		Course course=courseService.getCourse(course_id);
		return course.getCourse_semister();
	}
	
	@RequestMapping(value="getSubjectCode.json")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody String/*Map<String,String>*/ getSubjectCode(@RequestParam(value = "course_id", required = true) String course_id,
			@RequestParam(value = "semister", required = true) int sem,
			@RequestParam(value = "subject_id", required = true) String subject_id,
			ModelMap modelMap ){
		String subject_code=crs_subService.getSubjectCode(course_id, sem, subject_id);
		/*Map<String,String> mapSub=new HashMap<String,String>();
		mapSub.put("subject_code", subject_code);*/
		return subject_code;
	}
	
	//Add Assignment
	@RequestMapping(value = "assignments/add", method = RequestMethod.POST)
    public String addAssignment(@ModelAttribute("assignment") Assignment assignment, Model model) {
		try {
			assignment.setPending_practical(assignment.getAssigned_practical());
			assignment.setPending_theory(assignment.getAssigned_theory());
			
			String id=assignment.getAssignment_id();
			String new_id=id.replace(".", ",");
			assignment.setAssignment_id(new_id);
			
			SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
			String today = outFormat.format(new Date());
			assignment.setCreated_date(today);
			AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
	        String username=auth.getName();
	        assignment.setCreated_by(username);
	        assignment.setApproval_status("NEW");
	        assignment.setApproved_by("Not Approved");
	        assignment.setApproved_date(today);
			assignmentService.addAssignment(assignment);
			
		}
        catch(Exception ex) {
        	model.addAttribute("errorMsg", "Assignment Id already exist");
        	model.addAttribute("listCourse", courseService.getAllCourses());
			model.addAttribute("listFaculty", facultyService.getAllFaculties());
        	return "add_assignment";
        }
		return "redirect:/assignments";

		
    }
	
	//Show Edit Assignment Form
	@RequestMapping("editAssignment/{assignment_id}")
    public String editAssignmentForm(@PathVariable("assignment_id") String id, Model model) {
		Assignment assign=assignmentService.getAssignment(id);
		
		Date start=assign.getStart_date();
		SimpleDateFormat simpleStart = new SimpleDateFormat("MM/dd/yyyy");
		String formattedStart = simpleStart.format(start);
		model.addAttribute("start_date",formattedStart);
		Date end=assign.getEnd_date();
		SimpleDateFormat simpleEnd = new SimpleDateFormat("MM/dd/yyyy");
		String formattedEnd = simpleEnd.format(end);
		model.addAttribute("end_date",formattedEnd);
		model.addAttribute("assignment", assign);
	    return "edit_assignment";
    }
	
	
	//Edit Assignment Details
	@RequestMapping(value = "assignments/edit", method = RequestMethod.POST)
    public String editAssignment(@ModelAttribute("assignment") Assignment assignment) {
		SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
		String today = outFormat.format(new Date());
		assignment.setCreated_date(today);
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        String username=auth.getName();
        assignment.setCreated_by(username);
        assignment.setApproval_status("NEW");
        assignment.setApproved_by("Not Approved");
        assignment.setApproved_date(today);
		assignment.setPending_practical(assignment.getAssigned_practical());
		assignment.setPending_theory(assignment.getAssigned_theory());
		assignment.setApproval_status("NEW");
		
        assignmentService.updateAssignment(assignment);
        return "redirect:/assignments";
    }

	//Delete Assignment
	@RequestMapping("removeAssignment/{assignment_id}")
    public String removeAssignment(@PathVariable("assignment_id") String id) {
        assignmentService.removeAssignment(id);
        return "redirect:/assignments";
    }
	
	
	@RequestMapping("approveAssignment/{assignment_id}")
    public String approveAssignment(@PathVariable("assignment_id") String id, Model model) {
	        Assignment assign=assignmentService.getAssignment(id);
	        assign.setApproval_status("APPROVED");
		        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
				String today = outFormat.format(new Date());
				assign.setApproved_date(today);
				AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
		        String username=auth.getName();
		        assign.setApproved_by(username);
		        assignmentService.updateAssignment(assign);

	        return "redirect:/assignments";
	    }

		//Delete Assignment
		@RequestMapping("rejectAssignment/{assignment_id}")
	    public String rejectAssignment(@PathVariable("assignment_id") String id) {
			Assignment assignment=assignmentService.getAssignment(id);
	        assignment.setApproval_status("REJECTED");
	        SimpleDateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
			String today = outFormat.format(new Date());
			assignment.setApproved_date(today);
			AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
	        String username=auth.getName();
	        assignment.setApproved_by(username);
	        assignmentService.updateAssignment(assignment);
	        return "redirect:/assignments";
	    }
	
		//Show Assignment Form
				@RequestMapping("showAssignment/{assignment_id}")
				public String showAssignmentForm(@PathVariable("assignment_id") String id, Model model) {
					Assignment assign=assignmentService.getAssignment(id);
					if(!assign.equals(null)) {
						model.addAttribute("assign",assign);
						if(!assign.getCourse_id().equals(null)) {
							Course course=courseService.getCourse(assign.getCourse_id());
							model.addAttribute("course",course.getCourse_name());
						}
						else model.addAttribute("course","--");
						if(!assign.getCourse_id().equals(null) && assign.getSemister()!=0 && !assign.getSubject_id().equals(null)) {
							String subject_code=crs_subService.getSubjectCode(assign.getCourse_id(),assign.getSemister(),assign.getSubject_id());
							model.addAttribute("subject_code",subject_code);
						}
						else model.addAttribute("subject_code","--");
						if(!assign.getSubject_id().equals(null)) {
							Subject subject=subjectService.getSubject(assign.getSubject_id());
							model.addAttribute("subject",subject.getSubject_name());
						}
						else model.addAttribute("subject","--");
						if(!assign.getFaculty_id().equals(null)) {
							Faculty faculty=facultyService.getFaculty(assign.getFaculty_id());
							model.addAttribute("faculty",faculty.getFaculty_firstname()+" "+faculty.getFaculty_lastname());
						}
						else model.addAttribute("faculty","--");
						
					}
					return "show_assignment";
				}
				
				/*@RequestMapping(value="export_appointment_letter", method = RequestMethod.GET)
				public ModelAndView generatePdf(HttpServletRequest request,
				   HttpServletResponse response) throws Exception {

				  List<AttendanceReport> attReport=getListAttReport();
				  ModelAndView modelAndView = new ModelAndView("pdfView", "attReport",attReport);
				  
				  return modelAndView;
				 }
*/
				
				@RequestMapping("getAppointmentLetter/{assignment_id}")
			    public String getAppointmentLetter(@PathVariable("assignment_id") String assignment_id, Model model) {
				        Assignment assignment=assignmentService.getAssignment(assignment_id);
				        SimpleDateFormat outFormat = new SimpleDateFormat("dd.MM.yyyy");
						String today = outFormat.format(new Date());
						model.addAttribute("assignment", assignment);
						model.addAttribute("today", today);
						Faculty faculty=facultyService.getFaculty(assignment.getFaculty_id());
						model.addAttribute("faculty", faculty);
						model.addAttribute("faculty_name", faculty.getFaculty_firstname()+" "+faculty.getFaculty_lastname());
						Course course=courseService.getCourse(assignment.getCourse_id());
						model.addAttribute("course_name", course.getCourse_name());
						String subject_code=crs_subService.getSubjectCode(assignment.getCourse_id(), assignment.getSemister(), assignment.getSubject_id());
						model.addAttribute("subject_code", subject_code);
						Subject subject=subjectService.getSubject(assignment.getSubject_id());
						model.addAttribute("subject_name", subject.getSubject_name());
						int theory_pay=paymentService.getPaymentAmount("Theory");
						model.addAttribute("theory_pay", theory_pay);
						int prac_pay=paymentService.getPaymentAmount("Practical");
						model.addAttribute("prac_pay", prac_pay);
					
				        return "AppointmentLetter";
				    }

				
				
}

