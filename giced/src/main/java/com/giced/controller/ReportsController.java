package com.giced.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.giced.model.Assignment;
import com.giced.model.Attendance;
import com.giced.model.AttendanceMap;
import com.giced.model.AttendanceReport;
import com.giced.model.Course;
import com.giced.model.Faculty;
import com.giced.model.Subject;
import com.giced.service.AssignmentService;
import com.giced.service.AttendanceService;
import com.giced.service.CourseService;
import com.giced.service.CourseSubjectService;
import com.giced.service.FacultyService;
import com.giced.service.SubjectService;

@Controller
public class ReportsController {

	private AssignmentService assignmentService;
	private AttendanceService attendanceService;
	private CourseService courseService;
	private SubjectService subjectService;
	private FacultyService facultyService;
	private CourseSubjectService crs_subService;
	private Map<String, List<AttendanceMap>> mapReport;
	private String month;
	private int year;
	
	public Map<String, List<AttendanceMap>> getMapReport() {
		return mapReport;
	}

	public void setMapReport(Map<String, List<AttendanceMap>> mapReport) {
		this.mapReport = mapReport;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	@Autowired
	public void setCrs_subService(CourseSubjectService crs_subService) {
		this.crs_subService = crs_subService;
	}

	@Autowired
	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}

	@Autowired
	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	@Autowired
	public void setCourseService(CourseService courseService) {
		this.courseService = courseService;
	}

	@Autowired
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	@Autowired
	public void setFacultyService(FacultyService facultyService) {
		this.facultyService = facultyService;
	}

	
	@RequestMapping(value = "attendance_report", method = RequestMethod.POST)
    public String searchData(@ModelAttribute("attReport") AttendanceReport attReport, Model model) {
		List<Attendance> listAttendance=attendanceService.getAllAttendance();
		List<Attendance> listNewAttendance=new ArrayList<Attendance>();
		
		for(Attendance att:listAttendance) {
				Date date=att.getDate();
				SimpleDateFormat dfYear = new SimpleDateFormat("yyyy");
				int year = Integer.parseInt(dfYear.format(date));
				SimpleDateFormat dfMonth = new SimpleDateFormat("MM");
				int month = Integer.parseInt(dfMonth.format(date));
				if(month==attReport.getMonth() && year==attReport.getYear()) {
					listNewAttendance.add(att);
				}
			}
		
		Set<String> setAssignment=new HashSet<String>();
		
		for (Attendance att:listNewAttendance) {
        	setAssignment.add(att.getAssignment_id());
		}
		
        List<AttendanceReport> listReport=new ArrayList<AttendanceReport>();
        for(String id:setAssignment) {
        	Assignment ass=assignmentService.getAssignment(id);
        	AttendanceReport ar=new AttendanceReport();
        	Course course=courseService.getCourse(ass.getCourse_id());
			Subject subject = subjectService.getSubject(ass.getSubject_id());
			Faculty faculty=facultyService.getFaculty(ass.getFaculty_id());
			ar.setAssignment_id(ass.getAssignment_id());
			ar.setCourse(course.getCourse_name());
			ar.setSem(ass.getSemister());
			ar.setSub_code(crs_subService.getSubjectCode(ass.getCourse_id(), ass.getSemister(), ass.getSubject_id()));
			ar.setSubject(subject.getSubject_name());
			ar.setFaculty(faculty.getFaculty_firstname()+" "+ faculty.getFaculty_lastname());
			ar.setAssigned_practical(ass.getAssigned_practical());
			ar.setAssigned_theory(ass.getAssigned_theory());
			ar.setApproval_date(ass.getApproved_date());
			double total_theory=0.0;
        	double total_prac=0.0;
        	double completed_theory=0.0;
        	double completed_prac=0.0;
        	List<Attendance> listTemp=new ArrayList<Attendance>();
	        for(Attendance att: listNewAttendance) {
	        	if(ass.getAssignment_id().equals(att.getAssignment_id())) {
	        		listTemp.add(att);
	        	}
			}
	        for(Attendance att: listTemp) {
	        	if(ass.getAssignment_id().equals(att.getAssignment_id())) {
	        		if(att.getAssignment_type().equals("Theory")) {
	    				double l_completed= Double.parseDouble(att.getCompleted_hours());
	    				completed_theory=completed_theory+l_completed;
	    				total_theory=total_theory+att.getAmount();
	    			}
	    			else if(att.getAssignment_type().equals("Practical")) {
	    				double l_completed= Double.parseDouble(att.getCompleted_hours());
	    				completed_prac=completed_prac+l_completed;
	    				total_prac=total_prac+att.getAmount();
	    			}
	        	}
			}
	        
	        ar.setPending_practical(ass.getPending_practical());
			ar.setPending_theory(ass.getPending_theory());
	        ar.setTotal_theory(total_theory);
	        ar.setTotal_prac(total_prac);
	        ar.setCompleted_theory(String.valueOf(completed_theory));
	        ar.setCompleted_practical(String.valueOf(completed_prac));
	        double prev_theory=Double.parseDouble(ass.getPending_theory())+completed_theory;
	        double prev_prac=Double.parseDouble(ass.getPending_practical())+completed_prac;
	        ar.setPrev_bal_theory(String.valueOf(prev_theory));
	        ar.setPrev_bal_practical(String.valueOf(prev_prac));
	        
	        listReport.add(ar);
        }
        
        
        
        Map<String, List<AttendanceMap>> mapReport = new HashMap<String, List<AttendanceMap>>();
        
        
		
		for(String ass:setAssignment) {
			List<AttendanceMap> listMap = new ArrayList<AttendanceMap>();
			for (Attendance att : listNewAttendance) {
				if(att.getAssignment_id().equals(ass)) {
					AttendanceMap am=new AttendanceMap();
					am.setAssignment_id(ass);
					Date start=att.getDate();
					SimpleDateFormat simpleStart = new SimpleDateFormat("dd/MM/yyyy");
					String formattedDate = simpleStart.format(start);
					am.setDate(formattedDate);
					if(att.getAssignment_type().equals("Theory")) {
						am.setAssigned_theory(att.getCompleted_hours());
						am.setAssigned_practical("0");
					}
					if(att.getAssignment_type().equals("Practical")) {
						am.setAssigned_practical(att.getCompleted_hours());
						am.setAssigned_theory("0");
					}
					listMap.add(am);
				}
			}
			mapReport.put(ass, listMap);
		}
        
        model.addAttribute("listReport", listReport);
        model.addAttribute("mapReport", mapReport);
		model.addAttribute("attReport", attReport);
		
		SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String strMonth=null;
        try {
        	strMonth=monthDisplay.format(monthParse.parse(String.valueOf(attReport.getMonth())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("month", strMonth.toUpperCase());
        model.addAttribute("year", attReport.getYear());
		setMapReport(mapReport);
        setMonth(strMonth.toUpperCase());
        setYear(year);
		return "AttendanceReport";
    }
	
	
	@RequestMapping(value = "/get_report_page", method = RequestMethod.GET)
    public  String  getReportPage(Model model) {
		model.addAttribute("listCourse", courseService.getAllCourses());
		model.addAttribute("listSubject", subjectService.getAllSubjects());
		model.addAttribute("listFaculty", facultyService.getAllFaculties());
		model.addAttribute("attReport", new AttendanceReport());
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("user_role", auth.getAuthorities());
		return "attendance_report";

   }
	
	
}
