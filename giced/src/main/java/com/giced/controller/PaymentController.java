package com.giced.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.giced.model.Payment;
import com.giced.service.PaymentService;


@Controller
public class PaymentController {
	
	private PaymentService paymentService;
	
	@Autowired
	public void setPaymentService(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	//Show Payment List
	@RequestMapping(value = "payments", method = RequestMethod.GET)
	public String listPayment(Model model) {
	        
	        //model.addAttribute("appt", new Appointment_Type());
	        model.addAttribute("listPayment", paymentService.getAllPayments());
	        AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
	        model.addAttribute("user_role", auth.getAuthorities());
	        return "list_payment";
	 }
	
	//Show Add Payment Form
	@RequestMapping(value = "add_payment", method = RequestMethod.GET)
	public String addPaymentForm(Model model) {
		model.addAttribute("payment", new Payment());
		return "add_payment";
	}
	
	//Add Payment 
	@RequestMapping(value = "payments/add", method = RequestMethod.POST)
    public String addAppointment(@ModelAttribute("payment") Payment payment, Model model) {
		try {
			paymentService.addPayment(payment);
		}
        catch(Exception ex) {
        	model.addAttribute("errorMsg", "Payment Type already exist");
        	return "add_payment";
        }
        return "redirect:/payments";
    }
	
	//Show Edit Payment Form
	@RequestMapping("editPayment/{payment_id}")
    public String editAppointmentForm(@PathVariable("payment_id") int id, Model model) {
		model.addAttribute("payment", paymentService.getPayment(id));
		return "edit_payment";
    }
	
	//Edit Payment
	@RequestMapping(value = "payments/edit", method = RequestMethod.POST)
    public String editAppointment(@ModelAttribute("payment") Payment payment,
    		final RedirectAttributes redirectAttributes) {
        paymentService.updatePayment(payment);
        redirectAttributes.addFlashAttribute("msg", "Updated Successfully!");
        return "redirect:/payments";
    }

	//Delete Appointment
	@RequestMapping("removePayment/{payment_id}")
    public String removeAppointment(@PathVariable("payment_id") int id,
    		final RedirectAttributes redirectAttributes) {
        paymentService.removePayment(id);
        redirectAttributes.addFlashAttribute("msg", "Deleted Successfully!");
        return "redirect:/payments";
    }
	
	
}
