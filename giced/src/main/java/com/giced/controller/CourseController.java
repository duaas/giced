package com.giced.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.giced.model.Course;
import com.giced.model.Course_Subject;
import com.giced.model.Duration;
import com.giced.model.Subject;
import com.giced.model.Title;
import com.giced.service.CourseService;
import com.giced.service.CourseSubjectService;
import com.giced.service.DurationService;
import com.giced.service.SubjectService;
import com.giced.service.TitleService;

@Controller
public class CourseController {

	private DurationService durationService;
	private TitleService titleService;
	private SubjectService subjectService;
	private CourseService courseService;
	private CourseSubjectService crs_subService;
	
	@Autowired
	public void setCrs_subService(CourseSubjectService crs_subService) {
		this.crs_subService = crs_subService;
	}

	@Autowired
	public void setCourseService(CourseService courseService) {
		this.courseService = courseService;
	}

	@Autowired
	public void setDurationService(DurationService durationService) {
		this.durationService = durationService;
	}

	@Autowired
	public void setTitleService(TitleService titleService) {
		this.titleService = titleService;
	}

	@Autowired
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

		//Show Course List
		@RequestMapping(value = "courses", method = RequestMethod.GET)
		public String listCourses(Model model) {
		        model.addAttribute("course", new Course());
		        model.addAttribute("listCourse", courseService.getAllCourses());
		        model.addAttribute("listDuration", durationService.getAllDurations());
		        model.addAttribute("listTitle", titleService.getAllTitles());
		        model.addAttribute("listSubject",subjectService.getAllSubjects());
		        AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
		        model.addAttribute("user_role", auth.getAuthorities());
		        return "list_course";
		 }
		
		//Show Add Course Form
		@RequestMapping(value = "add_course", method = RequestMethod.GET)
		public String addUserForm(Model model) {
			Course course=new Course();
			model.addAttribute("listDuration", durationService.getAllDurations());
	        model.addAttribute("listTitle", titleService.getAllTitles());
	        model.addAttribute("listSubject",subjectService.getAllSubjects());
			model.addAttribute("course", course);
			return "add_course";
		}
		
		//Add Course 
		@RequestMapping(value = "courses/add", method = RequestMethod.POST)
	    public String addCourse(@ModelAttribute("course")Course course, Model model) {
			try {
				courseService.addCourse(course);
			}
	        catch(Exception ex) {
	        	model.addAttribute("errorMsg", "Course Id already exist");
	        	model.addAttribute("listDuration", durationService.getAllDurations());
		        model.addAttribute("listTitle", titleService.getAllTitles());
		        model.addAttribute("listSubject",subjectService.getAllSubjects());
	        	return "add_course";
	        }
	        return "redirect:/courses";
	    }
		
		//Show Edit Course Form
		@RequestMapping("editCourse/{course_id}")
	    public String editCourseForm(@PathVariable("course_id") String id, Model model) {
			Course course=courseService.getCourse(id);
			model.addAttribute("course",course);
			model.addAttribute("listDuration", durationService.getAllDurations());
	        model.addAttribute("listTitle", titleService.getAllTitles());
	        model.addAttribute("listSubject",subjectService.getAllSubjects());
			return "edit_course";
	    }
		
		//Show Add Course-Subject Form
				@RequestMapping("addSubjectforCourse/{course_id}")
			    public String addSubjectforCourse(@PathVariable("course_id") String id, Model model) {
					Course course=courseService.getCourse(id);
					model.addAttribute("course_id",course.getCourse_id());
					model.addAttribute("course_name",course.getCourse_name());
					model.addAttribute("sem",course.getCourse_semister());
					model.addAttribute("listSubject", subjectService.getAllSubjects());
			       return "add_course_subject";
			    }
				
				
				//Add Course Subject
				@RequestMapping(value = "courses/addCourseSubject", method = RequestMethod.POST)
			    public String addCourseSubject(@RequestParam("course_id") String course_id,@RequestParam("sem") int sem,
			    		@RequestParam(value="subject1", required=false) String subject1,@RequestParam(value="subject2", required=false) String subject2,
			    		@RequestParam(value="subject3", required=false) String subject3,@RequestParam(value="subject4", required=false) String subject4,
			    		@RequestParam(value="subject5", required=false) String subject5,@RequestParam(value="subject6",required=false) String subject6,
			    		@RequestParam(value="subject7", required=false) String subject7,@RequestParam(value="subject8", required=false) String subject8) {
					
					boolean crs_exist=crs_subService.existCourseSubject(course_id);
					if(crs_exist!=true)	crs_subService.deleteCourseSubject(course_id);
					
						String[] sub=subject1.split(",");
						int i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(1);
							crs.setSubject_id(s);
							crs.setSubject_code(1+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					
					
					
					sub=subject2.split(",");
					i=1;
					for(String s:sub) {
						Course_Subject crs=new Course_Subject();
						crs.setCourse_id(course_id);
						crs.setSemister(2);
						crs.setSubject_id(s);
						crs.setSubject_code(2+"."+i);
						i++;
						crs_subService.addCourseSubject(crs);
					}
					
					if(sem>2) {
					sub=subject3.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(3);
							crs.setSubject_id(s);
							crs.setSubject_code(3+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					
					
					sub=subject4.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(4);
							crs.setSubject_id(s);
							crs.setSubject_code(4+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					}
					
					if(sem>4) {
					sub=subject5.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(5);
							crs.setSubject_id(s);
							crs.setSubject_code(5+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					
					
					sub=subject6.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(6);
							crs.setSubject_id(s);
							crs.setSubject_code(6+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
				
					}
					
					if(sem>6) {
					sub=subject7.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(7);
							crs.setSubject_id(s);
							crs.setSubject_code(7+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					
					
					sub=subject8.split(",");
						i=1;
						for(String s:sub) {
							Course_Subject crs=new Course_Subject();
							crs.setCourse_id(course_id);
							crs.setSemister(8);
							crs.setSubject_id(s);
							crs.setSubject_code(8+"."+i);
							i++;
							crs_subService.addCourseSubject(crs);
						}
					}
					
				  return "redirect:/courses";
			    }
		
		//Show Course Form
		@RequestMapping("showCourse/{course_id}")
		public String showCourseForm(@PathVariable("course_id") String id, Model model) {
			Course course=courseService.getCourse(id);
			model.addAttribute("course",course);
			if(course.getCourse_duration()==null) model.addAttribute("duration","--");			
			else {
				Duration d=durationService.getDuration(course.getCourse_duration());
				if(d!=null)	model.addAttribute("duration",d.getDuration_name());		
				else model.addAttribute("duration","--");
			}
			if(course.getCourse_title()=="NONE") model.addAttribute("title","--");			
			else {
				Title t=titleService.getTitle(course.getCourse_title());
				if(t!=null)	model.addAttribute("title",t.getTitle_name());		
				else model.addAttribute("title","--");	
			}
			int sem=course.getCourse_semister();
			for(int i=1;i<=sem;i++) {
				List<Course_Subject> listCrsSub=crs_subService.getCourseSubject(id, i);
				
				List<Subject> listSubject=new ArrayList<Subject>();
				for(Course_Subject cs:listCrsSub ) {
					String s=cs.getSubject_id();
					Subject sub=subjectService.getSubject(s);
					sub.setSubject_id(cs.getSubject_code());
					listSubject.add(sub);
				}
				model.addAttribute("listSubject_"+i, listSubject);
			}
			model.addAttribute("sem", sem);
			
			
			return "show_course";
		}
		
		
		//Edit Course
		@RequestMapping(value = "courses/edit", method = RequestMethod.POST)
	    public String editCourse(@ModelAttribute("course") Course course, Model model) {
			courseService.updateCourse(course);
		    return "redirect:/courses";
	    }

		//Delete User
		@RequestMapping("removeCourse/{course_id}")
	    public String removeCourse(@PathVariable("course_id") String id) {
	       courseService.removeCourse(id);
	        return "redirect:/courses";
	    }
}
