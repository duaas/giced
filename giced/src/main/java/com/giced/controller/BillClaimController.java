package com.giced.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.giced.model.Assignment;
import com.giced.model.Attendance;
import com.giced.model.BillClaimReport;
import com.giced.model.Faculty;
import com.giced.service.AssignmentService;
import com.giced.service.AttendanceService;
import com.giced.service.FacultyService;

@Controller
public class BillClaimController {

	private AssignmentService assignmentService;
	private AttendanceService attendanceService;
	private FacultyService facultyService;
	private String month;
	private int year;
	
	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	private Map<String,List<Double>> mapBCR;
	
	public Map<String, List<Double>> getMapBCR() {
		return mapBCR;
	}

	public void setMapBCR(Map<String, List<Double>> mapBCR) {
		this.mapBCR = mapBCR;
	}

	@Autowired
	public void setAttendanceService(AttendanceService attendanceService) {
		this.attendanceService = attendanceService;
	}

	@Autowired
	public void setAssignmentService(AssignmentService assignmentService) {
		this.assignmentService = assignmentService;
	}

	@Autowired
	public void setFacultyService(FacultyService facultyService) {
		this.facultyService = facultyService;
	}

	
	@RequestMapping(value = "bill_claim_report", method = RequestMethod.POST)
    public String searchData(@ModelAttribute("bcReport") BillClaimReport bcReport, Model model) {
		
		List<Attendance> listAttendance=attendanceService.getAllAttendance();
		List<Attendance> listNewAttendance=new ArrayList<Attendance>();
		List<BillClaimReport> listBCR=new ArrayList<BillClaimReport>();
		
	    for(Attendance att:listAttendance) {
				Date date=att.getDate();
				SimpleDateFormat dfYear = new SimpleDateFormat("yyyy");
				int year = Integer.parseInt(dfYear.format(date));
				SimpleDateFormat dfMonth = new SimpleDateFormat("MM");
				int month = Integer.parseInt(dfMonth.format(date));
				if(month==bcReport.getMonth() && year==bcReport.getYear()) {
					listNewAttendance.add(att);
				}
			}
		
		for(Attendance att:listNewAttendance) {
			Assignment ass=assignmentService.getAssignment(att.getAssignment_id());
			Faculty faculty=facultyService.getFaculty(ass.getFaculty_id());
			//BillClaimReport bcr=new BillClaimReport(faculty.getFaculty_firstname()+" "+ faculty.getFaculty_lastname(),att.getAmount());
			BillClaimReport bcr=new BillClaimReport();
			bcr.setFaculty(faculty.getFaculty_firstname()+" "+ faculty.getFaculty_lastname());
			bcr.setAmount(att.getAmount());
			Date date=att.getDate();
			SimpleDateFormat dfYear = new SimpleDateFormat("yyyy");
			bcr.setYear(Integer.parseInt(dfYear.format(date)));
			SimpleDateFormat dfMonth = new SimpleDateFormat("MM");
			bcr.setMonth(Integer.parseInt(dfMonth.format(date)));
			listBCR.add(bcr);
		}
		
		
		Map<String, List<Double>> mapBCR = new HashMap<String, List<Double>>();
		Set<String> setFaculty=new HashSet<String>();
		
		for (BillClaimReport bcr : listBCR) {
			setFaculty.add(bcr.getFaculty());
		}
		
		for(String faculty:setFaculty) {
			List<Double> listAmount = new ArrayList<Double>();
			for (BillClaimReport bcr : listBCR) {
				if(bcr.getFaculty().equals(faculty)) {
					listAmount.add(bcr.getAmount());
				}
			}
			mapBCR.put(faculty, listAmount);
		}
		
		model.addAttribute("listReport", mapBCR);
		model.addAttribute("listFaculty", facultyService.getAllFaculties());
		model.addAttribute("bcReport", bcReport);
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("user_role", auth.getAuthorities());
        
        SimpleDateFormat monthParse = new SimpleDateFormat("MM");
        SimpleDateFormat monthDisplay = new SimpleDateFormat("MMMM");
        String strMonth=null;
        try {
        	strMonth=monthDisplay.format(monthParse.parse(String.valueOf(bcReport.getMonth())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        model.addAttribute("displayMonth", strMonth.toUpperCase());
        model.addAttribute("displayYear", bcReport.getYear());
        
        setMapBCR(mapBCR);
        setMonth(strMonth.toUpperCase());
        setYear( bcReport.getYear());
        return "bill_claim_report";
    }
	
	
	@RequestMapping(value="export_bill_pdf", method = RequestMethod.GET)
	public ModelAndView generatePdf(HttpServletRequest request,
	   HttpServletResponse response) throws Exception {
		Map<String, List<Double>> mapBCR=getMapBCR();
		ModelAndView modelAndView = new ModelAndView("pdfBillClaim", "mapBCR",mapBCR);
		modelAndView.addObject("month", getMonth());
		modelAndView.addObject("year", String.valueOf(getYear()));
	  
	  return modelAndView;
	 }
	
	
	@RequestMapping(value = "/get_bill_claim", method = RequestMethod.GET)
    public  String  getReportPage(Model model) {
		model.addAttribute("bcReport", new BillClaimReport());
		AbstractAuthenticationToken auth = (AbstractAuthenticationToken)SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("user_role", auth.getAuthorities());
		return "bill_claim_report";

   }
	
	@RequestMapping(value = "/get_bill_claim_pdf", method = RequestMethod.GET)
    public  String  getBillClaim(Model model) {
		Map<String, List<Double>> mapBCR=getMapBCR();
		model.addAttribute("listReport", mapBCR);
		model.addAttribute("month",getMonth());
		model.addAttribute("year",String.valueOf(getYear()));
		return "BillClaim";

   }
}
