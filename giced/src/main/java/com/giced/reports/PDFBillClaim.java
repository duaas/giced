package com.giced.reports;


import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


public class PDFBillClaim extends AbstractITextPdfView  {

	

	@SuppressWarnings("unchecked")
	@Override
	protected void buildPdfDocument(Map<String, Object> model, Document doc,
			PdfWriter writer, HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		// TODO Auto-generated method stub
		// get data model which is passed by the Spring container
        Map<String, List<Double>> mapBCR=(Map<String, List<Double>>) model.get("mapBCR");
        
        String month=(String) model.get("month");
        String year=(String) model.get("year");
        
        doc.add(new Paragraph("UNIVERSITY OF MUMBAI"));
        doc.add(new Paragraph("GARWARE INSTIUTE OF CAREERE EDUCATION AND DEVELOPMENT"));
        doc.add(new Paragraph("BILL CLAIM DETAILS OF THE VISITING FACULY FOR THE MONTH OF"));
        doc.add(new Paragraph(month+" "+year));
         
        PdfPTable table = new PdfPTable(6);
        table.setWidthPercentage(100.0f);
        table.setWidths(new float[] {1.0f, 3.0f, 2.0f, 2.0f, 1.0f, 1.0f});
        table.setSpacingBefore(10);
         
        // define font for table header row
        Font font = FontFactory.getFont(FontFactory.HELVETICA);
        font.setColor(BaseColor.WHITE);
         
        // define table header cell
        PdfPCell cell = new PdfPCell();
        //cell.setBackgroundColor(BaseColor.BLUE);
        cell.setPadding(5);
         
        // write table header
        cell.setPhrase(new Phrase("SR. NO", font));
        table.addCell(cell);
        
        cell.setPhrase(new Phrase("FACULTY", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("AMOUNT", font));
        table.addCell(cell);
 
        cell.setPhrase(new Phrase("TOTAL AMOUNT", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("CHEQUE NUMBER", font));
        table.addCell(cell);
         
        cell.setPhrase(new Phrase("SIGNATURE", font));
        table.addCell(cell);

        // iterate and display values
        int i=1;
        String strAmount=null;
        Double total=0.0;
        for (Map.Entry<String, List<Double>> entry : mapBCR.entrySet()) {
        	String key = entry.getKey();
            List<Double> values = entry.getValue();
            for(double d:values) {
            	strAmount=strAmount+"\n"+String.valueOf(d);
            	total=total+d;
            }
           
            table.addCell(String.valueOf(i));
            table.addCell(key);
            table.addCell(strAmount);
            table.addCell(String.valueOf(total));
            table.addCell("  ");
            table.addCell("  ");
            
            i++;
        }
         
        doc.add(table);
	}

}
