package com.giced.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.giced.dao.PaymentDao;

import com.giced.model.Payment;


@Service
@Component
public class PaymentServiceImpl implements PaymentService {
	
	@Autowired
	private PaymentDao paymentDao;
	
	@Autowired
	public PaymentDao getPaymentDao() {
		return paymentDao;
	}

	@Autowired
	public void setPaymentDao(PaymentDao paymentDao) {
		this.paymentDao = paymentDao;
	}

	@Override
	@Transactional
	public void addPayment(Payment payment) {
		paymentDao.addPayment(payment);
	}

	@Override
	@Transactional
	public void updatePayment(Payment payment) {
		paymentDao.updatePayment(payment);
	}

	@Override
	@Transactional
	public void removePayment(int id) {
		paymentDao.removePayment(id);
	}

	@Override
	@Transactional
	public Payment getPayment(int id) {
		return paymentDao.getPayment(id);
	}

	@Override
	@Transactional
	public List<Payment> getAllPayments() {
		return paymentDao.getAllPayments();
	}

	@Override
	@Transactional
	public int getPaymentAmount(String payment_type) {
		return paymentDao.getPaymentAmount(payment_type);
	}

	
}
