package com.giced.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.giced.dao.CourseSubjectDao;
import com.giced.model.Course_Subject;

@Service
@Component
public class CourseSubjectServiceImpl implements CourseSubjectService {
	
	@Autowired
	private CourseSubjectDao crs_subDao;

	@Autowired	
    public CourseSubjectDao getCrs_subDao() {
		return crs_subDao;
	}

	@Autowired
	public void setCrs_subDao(CourseSubjectDao crs_subDao) {
		this.crs_subDao = crs_subDao;
	}

	

	@Override
	@Transactional
	public void addCourseSubject(Course_Subject crs_sub) {
		crs_subDao.addCourseSubject(crs_sub);
	}

	@Override
	@Transactional
	public void updateCourseSubject(Course_Subject crs_sub) {
		crs_subDao.updateCourseSubject(crs_sub);
	}

	@Override
	@Transactional
	public void removeCourseSubject(int id) {
		crs_subDao.removeCourseSubject(id);
	}

	@Override
	@Transactional
	public List<Course_Subject> getCourseSubject(String course_id, int sem) {
		return crs_subDao.getCourseSubject(course_id, sem);
	}

	@Override
	@Transactional
	public String getSubjectCode(String course_id, int sem, String subject_id) {
		return crs_subDao.getSubjectCode(course_id, sem, subject_id);
	}

	@Override
	@Transactional
	public void deleteCourseSubject(String course_id) {
		crs_subDao.deleteCourseSubject(course_id);
	}

	@Override
	@Transactional
	public Boolean existCourseSubject(String course_id) {
		return crs_subDao.existsCourseSubject(course_id);
	}

}
