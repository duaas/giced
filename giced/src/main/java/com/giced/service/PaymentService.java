package com.giced.service;

import java.util.List;

import com.giced.model.Payment;



public interface PaymentService {

	void addPayment(Payment payment);

    void updatePayment(Payment payment);

    void removePayment(int id);

    Payment getPayment(int id);

    List<Payment> getAllPayments();
    
    int getPaymentAmount(String payment_type);
}
