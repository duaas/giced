package com.giced.service;

import java.util.List;

import com.giced.model.Course_Subject;



public interface CourseSubjectService {

void addCourseSubject(Course_Subject crs_sub);
	
	void updateCourseSubject(Course_Subject crs_sub);
	
	void removeCourseSubject(int id);
	
	void deleteCourseSubject(String course_id);

    List<Course_Subject> getCourseSubject(String course_id,int sem);
    
    String getSubjectCode(String course_id,int sem,String subject_id);
    
    Boolean existCourseSubject(String course_id);
}
