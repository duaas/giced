<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Appointment Letter</title>
<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
<script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

</head>
<body>
<div style="margin-right:100px;margin-left:100px;font-size:15px;" >
<table width="90%" >
	<tr>
		<td>
			<table width="90%" >
				<tr>
					<td><img alt="GICED" src="../resources/core/images/giced_logo.jpg" width="120px" height="120px"></td>
					<td>
					<div align="center">
					UNIVERSITY OF MUMBAI'S<br/>
					GARWARE INSTITUTE OF CAREER AND EDUCATION AND DEVELOPMENT<br/>
					(Autonomous since 2006)<br/>
					Vidyanagri, Kalina, Santacruz(East), Mumbai-400 098.<br/>
					Tel.:2653 02 58/59, Accts.2653 02 57, Exam Unit:2653 02 56<br/>
					E-mail: garware@giced.mu.ac.in<br/>
					Website: www.giced.edu.in<br/>
					</div>
					</td>
					<td><img alt="Mumbai University" src="../resources/core/images/mum_univ_logo.png" width="120px" height="120px"></td>
				</tr>
				<tr>
					<td colspan="3" width="100%">===============================================================================================================</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%">
				<tr>
					<td>No. <c:set var="sub_test" value="${fn:replace(assignment.assignment_id,'_','/')}"></c:set>
					        <c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
					   ${sub}
					</td>
					<td align="right">Date: ${today}</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><div>  </div></td></tr>
	<tr><td><div>  </div></td></tr>
	<tr>
		<td>
			<table width="90%">
				<tr>
					<td>
						To,
					</td>
				</tr>
				<tr>
					<td>
						${faculty_name},<br/>
						${faculty.faculty_building}, ${faculty.faculty_street},<br/>
						${faculty.faculty_area}, <br/>
						${faculty.faculty_station},<br/>
						${faculty.faculty_city} - ${faculty.faculty_pincode}.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	<tr><td><br/></td></tr>
	<tr>
		<td>
			<table width="90%">
					<tr>
						<td>
							<c:if test="${faculty.faculty_abbreviation == 'Mr.'}">
							Dear Sir,<br/>
							</c:if>
							<c:if test="${faculty.faculty_abbreviation == 'Mrs.' || 'Ms.'}">
							Dear Madam,<br/>
							</c:if>
						</td>
					</tr>
					<tr><td><br/></td></tr>
					<tr>
						<td>
							We have pleasure in inviting you as visiting faculty for the academic year ${revision_year} for teaching Programme as below.
						</td>
					</tr>
					<tr><td><br/></td></tr>
					<tr>
						<td>
							<b>Course: ${course_name}</b><br/>
							<b>Semester: ${assignment.semister}</b><br/>
							<b>Paper/Subject: ${subject_code}: ${subject_name}</b><br/>
							<b>Theory Hours: ${assignment.assigned_theory}</b><br/>
							<b>Workshop Hours: ${assignment.assigned_practical}</b><br/>
							<b>No. of Visits: ${assignment.no_of_visits}</b>
						</td>
					</tr>
			</table>
		</td>
	</tr>
	<tr><td><br/></td></tr>
	<tr>
		<td>
			<table width="90%">
				<tr>
					<td width="100%" align="justify">
						Please find a write up on credit system and evaluation process being implemented. You are requested to conduct
						3 assessments of 20 marks each progressively in the semester. Concerned teachers may show the compiled
						internal marksheet to students and get their initial on it.
						<br/><br/>
						<b>Along with your detailed study plan from this year onwards kindly submit the number of assignments to
						be taken by you, details of the assignments and marks allotted. Kindly ensure that internal marks are
						being counter signed by the students concerned before its submission to the exam department.</b>
						<br/><br/>
						You may kindly checkup with the course coordinator on periodic evaluations of the students in line with syllabus
						and credit system standards.
						<br/><br/>
						As a token of appreciation we would like to offer you an honorarium of Rs. ${theory_pay}/- per hour for theory and Rs. ${prac_pay}/-
						per hour for workshop towards conveyance and other out-of-pocket expenses.
						<br/><br/>
						You are requested to sign Muster for record of payment.
						<br/><br/>
						As per norms 75% attendance in classroom is implemented judiciously by the management you are requested to
						personally take and submit the attendance in the office on daily basis.
						<br/><br/>
						Study plan to be submitted by each faculty to the Course Coordinator within week from the date of receiving this
						letter. And the coordinator will compile the Handbook subsequently.
						<br/><br/>
						We would very much appreciate if you could please confirm your acceptance at an early date.
						<br/><br/>
						Thanking you.
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="90%">
				<tr>
					<td>Yours Sincerely,</td>
				</tr>
				<tr><td><br/></td></tr>
				<tr><td><br/></td></tr>
				<tr><td><br/></td></tr>
				<tr>
					<td>I/C DIRECTOR</td>
				</tr>
				<tr>
					<td>C.C. 1. Course Co-ordinator, 2. Sr. Accountant, 3. Office Copy</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>

<%-- <br/><br/>
<div align="right">
<a href="<c:url value='/export_appointment_letter'/>" id="export_appointment_letter" class="btn btn-primary">Download PDF</a>
<a class="btn btn-default" href="<c:url value="/assignments"/>">CANCEL</a>
</div> --%>
</body>
</html>