<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>REPORT</title>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="/resources/core/css/hello.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>

</head>
<body>
	<div style="margin-right:50px;margin-left:50px;font-size:15px;" >
		<table width="100%">
			<tr>
				<td>
					<div align="center">
							UNIVERSITY OF MUMBAI<br/>
							GARWARE INSTITUTE OF CAREER EDUCATION AND DEVELOPMENT<br/>
							BILL CLAIM DETAILS OF VISITING FACULTY FOR THE MONTH OF<br/>
							${month} ${year}
					</div>
				</td>
			</tr>
		</table>
	
		<c:if test="${!empty listReport}">
        	<c:forEach var="item" items="${listReport}">
        		<div class="table-responsive">
        		<hr class="dashed">
        	<table width="100%">
	        	<tr>
	        		<td>
	        			FACULTY NAME
	        		</td>
	        		<td>
	        			: ${item.faculty}
	        		</td>
	        		<td>
	        		</td>
	        		<td>
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			REFERENCE NO
	        		</td>
	        		<td>
	        			<c:set var="sub_test" value="${fn:replace(item.assignment_id,'_','/')}"></c:set>
						<c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
						: ${sub}
	        		</td>
	        		<td>
	        			DATED
	        		</td>
	        		<td>
	        			: ${item.approval_date}
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			COURSE
	        		</td>
	        		<td>
	        			: ${item.course}
	        		</td>
	        		<td>
	        			SEMESTER
	        		</td>
	        		<td>
	        			: ${item.sem}
	        		</td>
	        	</tr>
	        	<tr>
	        		<td>
	        			PAPER
	        		</td>
	        		<td>
	        			: ${item.sub_code} - ${item.subject}
	        		</td>
	        		<td>
	        		</td>
	        		<td>
	        		</td>
	        	</tr>
        	
        	</table>
        	<hr class="dashed">
        	<table style="margin-left:200px;" width="70%">
        		<tr>
        			<td>
        				THEORY/PRACTICAL DATE
        			</td>
        			<td>
        				THEORY HOUR
        			</td>
        			<td>
        				PRACTICAL HOUR
        			</td>
        		</tr>
        		<tr>
        			<td colspan="3"><hr class="dashed"></td>
        		</tr>
        		<c:forEach var="map" items="${mapReport}" >
        		<c:if test="${item.assignment_id == map.key}">
        		<c:forEach var="mapList" items="${map.value}" >
	          	<tr>
        			<td>
        				${mapList.date}
        			</td>
        			<td>
        				${mapList.assigned_theory}
        			</td>
        			<td>
        				${mapList.assigned_practical}
        			</td>
        		</tr>
        		</c:forEach>
        		</c:if>
        		</c:forEach> 
        		<tr>
        			<td colspan="3"><hr class="dashed"></td>
        		</tr>
        		<tr>
        			<td>
        				TOTAL HOURS ALLOTTED
        			</td>
        			<td>
        				${item.assigned_theory}
        			</td>
        			<td>
        				${item.assigned_practical}
        			</td>
        		</tr>
        		<tr>
        			<td>
        				PREVIOUS BALANCE HOURS
        			</td>
        			<td>
        				${item.prev_bal_theory}
        			</td>
        			<td>
        				${item.prev_bal_practical}
        			</td>
        		</tr>
        		<tr>
        			<td>
        				BILL CLAIMED HOURS
        			</td>
        			<td>
        				${item.completed_theory}
        			</td>
        			<td>
        				${item.completed_practical}
        			</td>
        		</tr>
        		<tr>
        			<td>
        				BALANCE HOURS
        			</td>
        			<td>
        				${item.pending_theory}
        			</td>
        			<td>
        				${item.pending_practical}
        			</td>
        		</tr>
        		<tr>
        			<td colspan="3"><hr class="dashed"></td>
        		</tr>
        		<tr>
        			<td>
        				TOTAL AMOUNT
        			</td>
        			<td>
        				Rs. ${item.total_theory}
        			</td>
        			<td>
        				Rs. ${item.total_prac}
        			</td>
        		</tr>
        		<tr>
        			<td colspan="3"><hr class="dashed"></td>
        		</tr>
        	</table>
        	
        		
  	   			</div>
	 		</c:forEach>
        </c:if> 
	</div>
</body>
</html>