<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Faculty Assignments</title>
<c:if test="${user_role == '[ROLE_SUPER_ADMIN]'}">
	<jsp:include page="fragments/super_admin_header.jsp" />
</c:if>
<c:if test="${user_role == '[ROLE_ADMIN]'}">
	<jsp:include page="fragments/admin_header.jsp" />
</c:if>
<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel=stylesheet type="text/css" />

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
<script>
$(document).ready(function() {
	  $('#example').DataTable();
	});
</script>
</head>
<body>
	
	<div class="container">
	<div class="row">
		
        
        <div class="col-md-12">
        <table width="100%">
			<tr>
				<td><h4>Faculty Assignment</h4></td>
				<td style="text-align: right;"><a href="<c:url value='/add_assignment'/>" class="btn btn-primary">ADD</a></td>
		    </tr>
    	</table>
    	
    	<!-- New Assignments for the Super Admin to be Approved -->
    	<c:if test="${user_role == '[ROLE_SUPER_ADMIN]'}">
	    	<c:if test="${!empty listNewAssignment}"> 
	        <div class="table-responsive">
	        	
	              <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
	                    <thead>
	                    	<tr>
			                   	<th>ID</th>
			                	<th>Course</th>
			                	<th>Semester</th>
			                	<th>Subject</th>
			                	<th>Faculty</th>
			                	<th>Theory Hours</th>
		                		<th>Practical Hours</th>
			                	<th>Status</th>
			                	<th>Show</th>
			                	<th>Approve</th>
			                	<th>Reject</th>
	                    	</tr>
	                    </thead>
	                    <tbody>
	    					<c:forEach items="${listNewAssignment}" var="assign">
				                <tr>
				                	<td>
					                	<c:set var="sub_test" value="${fn:replace(assign.assignment_id,'_','/')}"></c:set>
					                	<c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
					                	${sub}
				                	</td>
				                    <td>
					                    <c:forEach items="${listCourse}" var="course">
					                    <c:if test = "${assign.course_id == course.course_id}">
					           				 ${course.course_name}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>${assign.semister}</td>
				         			<td>
					                    <c:forEach items="${listSubject}" var="sub">
					                    <c:if test = "${assign.subject_id == sub.subject_id}">
					           				 ${sub.subject_name}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>
					                    <c:forEach items="${listFaculty}" var="fac">
					                    <c:if test = "${assign.faculty_id == fac.faculty_id}">
					           				 ${fac.faculty_firstname} ${fac.faculty_lastname}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>${assign.assigned_theory}</td>
			         				<td>${assign.assigned_practical}</td>
				         			<td>${assign.approval_status}</td>
				         			<td><p data-placement="top" data-toggle="tooltip" title="Show">
			                    	<a class="btn btn-primary btn-xs" href="<c:url value='/showAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-folder-open"></span></a></p></td>
			                    	<td><p data-placement="top" data-toggle="tooltip" title="Approve">
				                    <a class="btn btn-primary btn-xs" href="<c:url value='/approveAssignment/${assign.assignment_id}'/>"><span class="glyphicon glyphicon-ok"></span></a></p></td>
				                    <td><p data-placement="top" data-toggle="tooltip" title="Reject">
				                    <a class="btn btn-primary btn-xs" href="<c:url value='/rejectAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-remove"></span></a></p></td>
				                </tr>
	            			</c:forEach>
					    </tbody>
					</table>   
				   
	          </div>
        	</c:if>
		</c:if> 
		
		
		<!-- New/Rejected Assignment for the Admin to edit before Approval -->
		<c:if test="${user_role == '[ROLE_ADMIN]'}">
			<c:if test="${!empty listAssignment}">
	        <div class="table-responsive">
	        	
	              <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
	                    <thead>
	                    	<tr>
			                   	<th>ID</th>
			                	<th>Course</th>
			                	<th>Semester</th>
			                	<th>Subject</th>
			                	<th>Faculty</th>
			                	<th>Theory Hours</th>
		                		<th>Practical Hours</th>
			                	<th>Status</th>
			                	<th>Show</th>
			                	<th>Edit</th>
			                	<th>Delete</th>
	                    	</tr>
	                    </thead>
	                    <tbody>
	    					<c:forEach items="${listAssignment}" var="assign">
				                <tr>
				                	<td>
					                	<c:set var="sub_test" value="${fn:replace(assign.assignment_id,'_','/')}"></c:set>
					                	<c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
					                	${sub}
				                	</td>
				                    <td>
					                    <c:forEach items="${listCourse}" var="course">
					                    <c:if test = "${assign.course_id == course.course_id}">
					           				 ${course.course_name}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>${assign.semister}</td>
				         			<td>
					                    <c:forEach items="${listSubject}" var="sub">
					                    <c:if test = "${assign.subject_id == sub.subject_id}">
					           				 ${sub.subject_name}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>
					                    <c:forEach items="${listFaculty}" var="fac">
					                    <c:if test = "${assign.faculty_id == fac.faculty_id}">
					           				 ${fac.faculty_firstname} ${fac.faculty_lastname}
					         			</c:if>
					         			</c:forEach>
				         			</td>
				         			<td>${assign.assigned_theory}</td>
			         				<td>${assign.assigned_practical}</td>
				         			<td>${assign.approval_status}</td>
				         			<td><p data-placement="top" data-toggle="tooltip" title="Show">
			                    	<a class="btn btn-primary btn-xs" href="<c:url value='/showAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-folder-open"></span></a></p></td>
			                    	<td><p data-placement="top" data-toggle="tooltip" title="Edit"><a class="btn btn-primary btn-xs" href="<c:url value='/editAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-pencil"></span></a></p></td>
				                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a class="btn btn-danger btn-xs" href="<c:url value='/removeAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-trash"></span></a></p></td>
				                </tr>
	            			</c:forEach>
					    </tbody>
					</table>   
				   
	          </div>
	      	</c:if>   
		</c:if>
		
		
       <c:if test="${!empty listApprovedAssignment}">
        <div class="table-responsive">
        	
              <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                    	<tr>
		                   	<th>ID</th>
		                	<th>Course</th>
		                	<th>Semester</th>
		                	<th>Subject</th>
		                	<th>Faculty</th>
		                	<th>Theory Hours</th>
		                	<th>Practical Hours</th>
		                	<th>Status</th>
		                	<th>Show</th>
		                	<th>Print</th>
		                	<th>Delete</th>
                    	</tr>
                    </thead>
                    <tbody>
    					<c:forEach items="${listApprovedAssignment}" var="assign">
			                <tr>
			                	<td>
				                	<c:set var="sub_test" value="${fn:replace(assign.assignment_id,'_','/')}"></c:set>
				                	<c:set var="sub" value="${fn:replace(sub_test,',','.')}"></c:set>
				                	${sub}
				                </td>
			                    <td>
				                    <c:forEach items="${listCourse}" var="course">
				                    <c:if test = "${assign.course_id == course.course_id}">
				           				 ${course.course_name}
				         			</c:if>
				         			</c:forEach>
			         			</td>
			         			<td>${assign.semister}</td>
			         			<td>
				                    <c:forEach items="${listSubject}" var="sub">
				                    <c:if test = "${assign.subject_id == sub.subject_id}">
				           				 ${sub.subject_name}
				         			</c:if>
				         			</c:forEach>
			         			</td>
			         			<td>
				                    <c:forEach items="${listFaculty}" var="fac">
				                    <c:if test = "${assign.faculty_id == fac.faculty_id}">
				           				 ${fac.faculty_firstname} ${fac.faculty_lastname}
				         			</c:if>
				         			</c:forEach>
			         			</td>
			         			<td>${assign.assigned_theory}</td>
			         			<td>${assign.assigned_practical}</td>
			         			<td>${assign.approval_status}</td>
			         			<td><p data-placement="top" data-toggle="tooltip" title="Show">
			                    <a class="btn btn-primary btn-xs" href="<c:url value='/showAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-folder-open"></span></a></p></td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Appointment Letter"><a class="btn btn-primary btn-xs" href="<c:url value='/getAppointmentLetter/${assign.assignment_id}'/>"  target="_blank" ><span class="glyphicon glyphicon-print"></span></a></p></td>
			                    <td><p data-placement="top" data-toggle="tooltip" title="Delete"><a class="btn btn-danger btn-xs" href="<c:url value='/removeAssignment/${assign.assignment_id}'/>" ><span class="glyphicon glyphicon-trash"></span></a></p></td>
			                </tr>
            			</c:forEach>
				    </tbody>
				</table>   
			   
          </div>
          
          </c:if>   
        </div>
	</div>
</body>
</html>