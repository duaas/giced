<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add New Payment</title>

<link href="<c:url value="/resources/core/css/bootstrap.min.css"/>" rel="stylesheet" />
    <script>
    function validateform(){  
		var payment_type=document.PaymentForm.payment_type.value;  
		var payment_amount=document.PaymentForm.payment_amount.value;  
		
		if (payment_type==null || payment_type==""){  
		  alert("Payment Type can't be blank.");  
		  return false;  
		}
		if (isNan(payment_amount)){  
			  alert("Please enter valid Payment Amount.");  
			  return false;  
		}
	}  
    </script>
</head>
<body>

<c:url var="post_url"  value="/payments/add" />
<form:form action="${post_url}" modelAttribute="payment" method="post" 
class="form-horizontal" name="PaymentForm" onsubmit="return validateform()">
<div class="row" >
	    		<div class="col-md-8 col-md-offset-1">
	      			<fieldset>
	      				<legend>Add New Payment Info</legend>
				      	<div class="form-group">
				            <div class="col-sm-4">
				            	<form:label path="payment_type">
				                    <spring:message text="Payment Type"/><span class="required">*</span>
				                </form:label>
				              	<form:input path="payment_type" name="payment_type" class="form-control required" />
				            </div>
			          	</div>
			          	<div class="form-group">
				            <div class="col-sm-4">
				            	<form:label path="payment_amount">
				                    <spring:message text="Payment Amount"/><span class="required">*</span>
				                </form:label>
				              	<form:input path="payment_amount" name="payment_amount" class="form-control required"/>
				            </div>
			          	</div>
				      	<c:if test="${not empty errorMsg}">
    					<div class="alert alert-danger">
    							<c:out value="${errorMsg}"/>
    						</div>
    					</c:if>
    					<div class="form-group">
				            <div class="col-sm-5 col-sm-offset-1">
				              <div class="pull-left">
				                <button type="submit" class="btn btn-primary">SAVE</button>
				                <a class="btn btn-default" href="<c:url value="/payments"/>">CANCEL</a>
				              </div>
				            </div>
			           </div>
	      			</fieldset>
				</div>
			</div>
</form:form>
</body>
</html>