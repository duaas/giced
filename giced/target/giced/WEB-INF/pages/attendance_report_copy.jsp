<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Attendance</title>
<c:if test="${user_role == '[ROLE_SUPER_ADMIN]'}">
	<jsp:include page="fragments/super_admin_header.jsp" />
</c:if>
<c:if test="${user_role == '[ROLE_ADMIN]'}">
	<jsp:include page="fragments/admin_header.jsp" />
</c:if>

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>

<script type="text/javascript">

('#exportPDF').onclick = function() {
	  val list=$('#attReport').val();
	  alert(list);
	 
    	 $.getJSON(
                 "export_pdf.json", 
                 {attReport: list},
                 function(data) {
                	 alert("Opening PDF.");
                 }
              );
    
    return false;
  }
  
function validateform(){  
	var month=document.ReportForm.month.value;  
	var year=document.ReportForm.year.value;  
	
	if (isNan(year)){  
		  alert("Please enter valid Year.");  
		  return false;  
	}
	if (month=="NONE"){  
		  alert("Please select valid Month.");  
		  return false;  
	}
}
</script>
<script>
$(document).ready(function() {
	  $('#example').DataTable();
	});
</script>

</head>
<body>

<c:url var="post_url"  value="/attendance_report" />
<form:form action="${post_url}" method="post" modelAttribute="attReport"
class="form-horizontal" name="ReportForm" onsubmit="return validateform()" >
<div class="row" >
    <div class="col-md-8 col-md-offset-1">
      <fieldset>
      	<legend>Reports</legend>
      	
           
	          <div class="form-group">
	          	<div class="col-sm-3">
					<label>Year</label>
					<form:input id="year" path="year" name="year" class="form-control" />
				</div>
	          	<div class="col-sm-3">
	            	<label>Month</label>
		            <form:select path="month" name="month" id="month" class="form-control" > 
						<option value="0" label="--- Select ---" />
						<option value="1" label="Jan" />
						<option value="2" label="Feb" />
						<option value="3" label="Mar" />
						<option value="4" label="Apr" />
						<option value="5" label="May" />
						<option value="6" label="Jun" />
						<option value="7" label="Jul" />
						<option value="8" label="Aug" />
						<option value="9" label="Sep" />
						<option value="10" label="Oct" />
						<option value="11" label="Nov" />
						<option value="12" label="Dec" />
					</form:select>
				</div>				
	        </div>
	          

         
          <legend></legend>
          <!-- Command -->
          <div class="form-group">
            <div class="col-sm-12">
              <div class="pull-right">
                <button type="submit" class="btn btn-primary">SEARCH</button>
                <a class="btn btn-default" href="<c:url value="/get_report_page"/>">CANCEL</a>
              </div>
            </div>
          </div>
          
         <div>Report for ${displayMonth} ${displayYear}</div>
           <c:if test="${!empty listReport}">
        <div class="table-responsive">
        	
              <table  id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" >
                    <thead>
                    	<tr>
                    	    <th>REFERENCE NO</th>
                    		<th>FACULTY</th>
		                   	<th>COURSE</th>
		                	<th>PAPER</th>
		                	<th>SEMESTER</th>
		                	<th>DATE</th>
		                	<th>THEORY HOUR</th>
		                	<th>PRACTICAL HOUR</th>
		                	<th>TOTAL AMOUNT</th>
		                </tr>
                   </thead>
                   <tbody>
                   	
                    	<c:forEach items="${listReport}" var="list">
                    	<tr>
                    		<td>${list.assignment_id}</td>
                    		<td>${list.faculty}</td>
	    					<td>${list.course}</td>
					        <td>${list.subject}</td>
					        <td>${list.sem}</td>
					        <td>${list.date}</td>
					        <!-- Theory Hour Allotment -->
					        <td>TOTAL HOURS ALLOTED	${list.assigned_theory}<br/>
					        PREVIOUS BALANCE HOURS	${list.prev_bal_theory}<br/>
					        BILL CLAIMED HOURS		${list.completed_theory}<br/>
					        BALANCE HOURS			${list.pending_theory}
					        </td>
						    <!-- Practical Hour Allotment -->
					        <td>TOTAL HOURS ALLOTED	${list.assigned_practical}<br/>
					        PREVIOUS BALANCE HOURS	${list.prev_bal_practical}<br/>
					        BILL CLAIMED HOURS		${list.completed_practical}<br/>
					        BALANCE HOURS			${list.pending_practical}
					        </td>
					        <td>${list.total_amount}</td>
						</tr>
						</c:forEach>
                   </tbody>
				</table>   
			   
          </div>
          
          <div class="form-group">
            <div class="col-sm-12">
              <div class="pull-right">
                <%-- <a href="<c:url value='/export_pdf'/>" class="btn btn-primary">Export to PDF</a> --%>
                <a href="<c:url value='/export_pdf'/>" id="exportPDF" class="btn btn-primary">Download PDF</a>
                <a href="<c:url value='/export_excel'/>" class="btn btn-primary">Download Excel</a>
              </div>
            </div>
          </div>
        </c:if> 
      </fieldset>
	</div>
</div>


</form:form>


</body>
</html>